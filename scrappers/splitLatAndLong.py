import csv
import uuid
yellowtaxifd = open('../datasets/twomillionwithfirstcolumn.csv')
yellowtaxibridge = csv.DictReader(yellowtaxifd)
impHeaders = ['locationID','pickup_longitude','pickup_latitude','dropoff_latitude','dropoff_longitude', 'trip_distance']

uberCoordfd = open('../datasets/uberCoOrdinates.csv', 'w')
uberCoordBridge = csv.DictWriter(uberCoordfd,impHeaders)
uberCoordBridge.writeheader()

uberCoOrds = {}

for data in yellowtaxibridge:
    if data[impHeaders[1]] == "0.0" or data[impHeaders[2]] == "0.0" or data[impHeaders[3]] == "0.0" or data[impHeaders[4]] == "0.0":
        continue
    else:
        uberCoOrds = {}
        uberCoOrds['locationID'] = str(uuid.uuid1())
        uberCoOrds[impHeaders[1]] = data[impHeaders[1]]
        uberCoOrds[impHeaders[2]] = data[impHeaders[2]]
        uberCoOrds[impHeaders[3]] = data[impHeaders[3]]
        uberCoOrds[impHeaders[4]] = data[impHeaders[4]]
        uberCoOrds[impHeaders[5]] = data[impHeaders[5]]
        uberCoordBridge.writerow(uberCoOrds)