import csv
from uber_rides.session import Session
from uber_rides.client import UberRidesClient
import random
serverToken = ["rrju0jQzyLMNrJoDyScq6cHQI_IvMwvlMpRZ5uoG","I6iiXAHhroHhmpsG5BI70BXK9bf2Fjj2B3j1lD1l",
               "s3sUcXPDQBvRGEF-YDfqjo-_q3a9O7Oqmvc4XyZm", "etffME6fWMViyChi49utKZ7Lr0fFNb0EFue18fYu",
               "s1dzhSUjsh8wPatjixJ2vMGamjhvYk2kBwMei7bg", "aGakjSGbxiQmG2uqikvdcbTDyLeT8dDE1MpQ4WmL",
               "LghkCCM5NCEotMm9EHXNfOa3NXGqqLY16vvTYfmp", "CmUR5iJcFDMsxrdFiPz40OlKwyfBEVY0iTwOuoHg"]
#rideshare,uberx, uberxl, uberblack, suv, or taxi.
headers = ['low_estimate', 'duration', 'product_id', 'high_estimate', 'estimate', 'distance']
#samplefd = open('../datasets/samplecsv.csv', 'r')

latLongDatafd = open('../datasets/uberCoOrdinates.csv', 'r')
latLongDataBridge = csv.DictReader(latLongDatafd)


ridesharefd = open('../datasets/uberRideshareCars.csv', 'w')
ridesharebridge = csv.DictWriter(ridesharefd, headers)
ridesharebridge.writeheader()

uberxlfd = open('../datasets/uberXLCars.csv', 'w')
uberxlfdbridge = csv.DictWriter(uberxlfd, headers)
uberxlfdbridge.writeheader()

uberxfd = open('../datasets/uberXCars.csv', 'w')
uberxfdbridge = csv.DictWriter(uberxfd, headers)
uberxfdbridge.writeheader()

blackfd = open('../datasets/uberBlackCars.csv', 'w')
blackfdbridge = csv.DictWriter(blackfd, headers)
blackfdbridge.writeheader()

suvfd = open('../datasets/uberSUVCars.csv', 'w')
suvfdbridge = csv.DictWriter(suvfd, headers)
suvfdbridge.writeheader()



latlongheaders = ['locationID', 'dropoff_latitude', 'dropoff_longitude', 'pickup_longitude', 'pickup_latitude']
#headers = ['low_estimate', 'duration', 'product_id', 'high_estimate', 'estimate', 'distance']
tempDict = {}
row = 0
for entry in latLongDataBridge:
    #print(entry['locationID'])
    #print(entry[latlongheaders[3]], entry[latlongheaders[4]])
    temp = random.randint(0,6)
    session = Session(server_token=serverToken[temp])
    client = UberRidesClient(session)
    row = row + 1
    print(row)
    start_longitude = float(entry[latlongheaders[3]])
    start_latitude = float(entry[latlongheaders[4]])

    end_longitude = float(entry[latlongheaders[2]])
    end_latitude = float(entry[latlongheaders[1]])

    # LIST OF DIFFERENT PRODUCTS OFFERED BY UBER
    ridesharelist = []
    uberxlist = []
    uberxllist = []
    uberblacklist = []
    suvlist = []

    queryString = client.get_price_estimates(
        start_longitude=start_longitude,
        start_latitude=start_latitude,

        end_latitude=end_latitude,
        end_longitude=end_longitude,
        seat_count=2,
    )

    groupString = client.get_products(latitude=start_latitude, longitude=start_longitude)
    products = groupString.json.get('products')

    for product in products:
        if 'product_group' not in product.keys():
            continue

        if product['product_group'] == "uberx":
            uberxlist.append(product['display_name'])

        elif product['product_group'] == "uberxl":
            uberxllist.append(product['display_name'])

        elif product['product_group'] == "uberblack":
            uberblacklist.append(product['display_name'])

        elif product['product_group'] == "rideshare":
            ridesharelist.append(product['display_name'])

        elif product['product_group'] == "suv":
            suvlist.append(product['display_name'])

    estimate = queryString.json.get('prices')

    for data in estimate:
        if data['display_name'] in ridesharelist:
            print("Inside rideshare")
            rideshareDict = {}
            rideshareDict[headers[0]] = data[headers[0]]
            rideshareDict[headers[1]] = data[headers[1]]
            rideshareDict[headers[2]] = data[headers[2]]
            rideshareDict[headers[3]] = data[headers[3]]
            rideshareDict[headers[4]] = data[headers[4]]
            rideshareDict[headers[5]] = data[headers[5]]
            ridesharebridge.writerow(rideshareDict)

        elif data['display_name'] in uberxllist:
            print("Inside X")
            XLDict = {}
            XLDict[headers[0]] = data[headers[0]]
            XLDict[headers[1]] = data[headers[1]]
            XLDict[headers[2]] = data[headers[2]]
            XLDict[headers[3]] = data[headers[3]]
            XLDict[headers[4]] = data[headers[4]]
            XLDict[headers[5]] = data[headers[5]]
            uberxlfdbridge.writerow(XLDict)

        elif data['display_name'] in uberblacklist:
            print("Inside black")
            BlackDict = {}
            BlackDict[headers[0]] = data[headers[0]]
            BlackDict[headers[1]] = data[headers[1]]
            BlackDict[headers[2]] = data[headers[2]]
            BlackDict[headers[3]] = data[headers[3]]
            BlackDict[headers[4]] = data[headers[4]]
            BlackDict[headers[5]] = data[headers[5]]
            blackfdbridge.writerow(BlackDict)

        elif data['display_name'] in suvlist:
            print("Inside suv")
            suvDict = {}
            suvDict[headers[0]] = data[headers[0]]
            suvDict[headers[1]] = data[headers[1]]
            suvDict[headers[2]] = data[headers[2]]
            suvDict[headers[3]] = data[headers[3]]
            suvDict[headers[4]] = data[headers[4]]
            suvDict[headers[5]] = data[headers[5]]
            suvfdbridge.writerow(suvDict)

        elif data['display_name'] in uberxlist:
            print("Inside XL")
            XDict = {}
            XDict[headers[0]] = data[headers[0]]
            XDict[headers[1]] = data[headers[1]]
            XDict[headers[2]] = data[headers[2]]
            XDict[headers[3]] = data[headers[3]]
            XDict[headers[4]] = data[headers[4]]
            XDict[headers[5]] = data[headers[5]]
            uberxfdbridge.writerow(XDict)
