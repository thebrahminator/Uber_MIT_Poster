import csv
from uber_rides.session import Session
from uber_rides.client import UberRidesClient
from scrappers.scrapperinit import serverToken

session = Session(server_token=serverToken)
client = UberRidesClient(session=session)

headings = ('startlat','startlong', 'endlat', 'endlong', 'SELECT', 'uberXL',
            'BLACK', 'SUV', 'ASSIST', 'WAV', 'POOL', 'uberX', 'TAXI')

