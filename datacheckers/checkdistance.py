import csv

distancefd = open('../datasets/yellow_tripdata_2016-01.csv', 'r')
distancefdbridge = csv.DictReader(distancefd)

test = 0
for data in distancefdbridge:
    if float(data['trip_distance']) > 75:
        test = test + 1

print(test)
