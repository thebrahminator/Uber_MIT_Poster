import pandas as pd
from sklearn.model_selection import train_test_split
from sklearn.preprocessing import MinMaxScaler
from sklearn.ensemble import RandomForestRegressor
import pickle

## Loading csv input data
def load_data():
    return pd.read_csv('../datasets/yellow_taxi.csv')

##Normalizing data and splitting into train and test
##Input and output features are assigned here too
def split_data(df):
    #scaler = MinMaxScaler()
    #scaler.fit(df)
    #MinMaxScaler(copy=True, feature_range=(0, 1))
    #scaler.transform(df)
    df.dropna()
    train, test = train_test_split(df, test_size=0.20,random_state=100)
    train_x = train[['pickup_longitude','pickup_latitude','dropoff_latitude','dropoff_longitude']]
    train_y = train['total_amount']
    test_x = test[['pickup_longitude','pickup_latitude','dropoff_latitude','dropoff_longitude']]
    test_y = test['total_amount']
    return train_x, train_y, test_x, test_y

clf = RandomForestRegressor(n_estimators=200, max_features='auto', n_jobs=-1)

def body():
    ##Calling the above functions
    df = load_data()
    tr_x, tr_y, ts_x, ts_y = split_data(df)

    #Previewing the data
    #print(tr_x.head())

    ##Training a Random Forest Regressor with 500 Trees
    #print('Training Started')

    #Fitting the training data
    print("Starting to train the data")
    clf.fit(tr_x, tr_y)
    print('Training Finished')

    #Training Accuracy
    print("Training Score")
    print(clf.score(tr_x, tr_y))

    #Test Accuracy
    print("Test Score")
    print(clf.score(ts_x, ts_y))

    return 1

# Predict the number of likes of any youtube video by providing the four features- comment, dislike, views and video popularity
def forever(x1,x2,x3,x4):
   return clf.predict([[x1,x2,x3,x4]])
body()

with open(r"../datasets/model.pickle", "wb") as output_file:
    pickle.dump(clf, output_file)